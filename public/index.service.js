angular.module('adsquizresults').factory('Answers', function($resource){
        
    var AnswersTry = $resource('/answers');
    var Quizzes = $resource('/quizzes');
    
    var getAnswers = function () {
        return Answers;
    };
    
    var query = function () {
        return AnswersTry.get().$promise;
    };

    var create = function (quiz, callback) {
        callback = callback || angular.noop;
        return Quizzes.save(quiz, function () {
            callback();
        }, function (err) {
            callback (err);
        }).$promise;
    };

    return {
        create: create,
        getAnswers: getAnswers,
        query: query,
    };
});


