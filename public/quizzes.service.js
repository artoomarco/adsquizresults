angular.module('adsquizresults').factory('Quiz', function ($resource) {
	var Quizzes = $resource('/quizzes');

	var create = function (quiz, callback) {
        console.log(quiz);
        callback = callback || angular.noop;
        return Quizzes.save(quiz, function () {
            callback();
        }, function (err) {
            callback (err);
        }).$promise;
    };

    return {
    	create: create,
    }
})