angular.module('adsquizresults').controller('AnswerCtrl', function ($scope, $rootScope, Answers, Quiz){
    //$scope.answers = Answers.getAnswers();
    Answers.query().then(function (answers) {
        $scope.answers = answers.answers;
        $scope.totale = answers.answers.length;
        //console.log(answers);
    }).catch();

    $scope.correctanswer = null;
    //$scope.totale = $scope.answers.length;
    $scope.punteggio = 0; 
      
    $scope.create = function (quiz, formcontact) {
        Quiz.create(quiz).then(function () {
            console.log('Quiz Creato'); 
        }).catch(function (err) {
            console.log(err);
            alert('Errore ' + err.status + ' ' + err.data.message);
        });
    }; 
    
    function sommaPunteggio() {
        $scope.punteggio = 0;
        for (var i in $scope.answers) {
            if ($scope.answers[i].correct == "YES")
                $scope.punteggio++;
        }
        console.log($scope.punteggio);
    }
    
    $scope.getNumber = function (no) {
        console.log(this);
        if ((this.quiz.answer[no]).toUpperCase().trim() == (this.n.resp).toUpperCase().trim()) {
            this.n.correct = "YES";
            this.n.points = 1;
        } else {
            this.n.correct = "NO";
            this.n.points = 0;
        }
        sommaPunteggio();
    };
    
})