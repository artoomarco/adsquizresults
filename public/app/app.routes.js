angular.module('adsquizresults').config(function ($stateProvider, $urlRouterProvider) {
	
	$urlRouterProvider.otherwise('/');

	$stateProvider
		.state('home', {
			url:'/',
			templateUrl:'../home/home.template.html',
			controller:'AnswerCtrl',
		})
		.state('results', {
			url:'/quizzes',
			templateUrl:'../quizzes/quizzes.template.html',
			controller:'QuizCtrl',
		})
});