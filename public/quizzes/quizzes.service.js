angular.module('adsquizresults').factory('Quiz', function ($resource) {
	var Quizzes = $resource('/quizzes');

    var query = function (callback) {
        callback = callback || angular.noop;
        return Quizzes.get(function (quizzes) {
            return callback(quizzes);
        }, function (err) {
            return callback(err);
        }).$promise;
    };

	var create = function (quiz, callback) {
        callback = callback || angular.noop;
        return Quizzes.save(quiz, function () {
            callback();
        }, function (err) {
            callback (err);
        }).$promise;
    };

    return {
    	create: create,
        query: query,
    }
})