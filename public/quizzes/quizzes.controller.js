angular.module('adsquizresults').controller('QuizCtrl', function ($scope, Quiz) {
	
	var loadQuiz = function () {
		Quiz.query().then(function (quizzes) {
			$scope.quizzes = quizzes.quizzes
			console.log(quizzes.quizzes);
		}).catch();
	}

	$scope.create = function (quiz) {
        Quiz.create(quiz).then(function () {
            console.log('Quiz Creato');
        }).catch();
    };  

    loadQuiz();
})