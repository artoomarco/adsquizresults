var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var quizSchema = new Schema({
    first_name          : {type: String, required: true },
    last_name           : {type: String, required: true },
    total               : Number,
    score               : Number,
    answer        	    : [{}],
    date                : { type: Date, default: Date.now }
});

var quiz = mongoose.model('quiz', quizSchema);

module.exports = {
	Quiz: quiz,
};