console.log('Start server?');

var express = require('express')
  ,bodyParser = require('body-parser')
 // , routes = require('./routes')
  , quizzes = require('./routes/quizzes')
  , answers = require('./routes/answers')
  , path = require('path')
  , mongoose = require('mongoose');

// MongoDB Connection 
mongoose.connect('mongodb://10.1.1.45:27017/adsquizresults');

var app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());



// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.use(express.static(path.join(__dirname, '..' ,'public')));

app.listen(app.get('port'), function(){
  console.log("Express server listening on port 3000.");

})

app.get('/', function(req, res){
  res.sendFile(path.join(__dirname, '..' ,'public', 'index.html'));
});


//app.get('/', routes.index);
app.post('/quizzes', quizzes.create);
app.get('/quizzes', quizzes.index);
app.get('/quizzes/last', quizzes.last);
app.get('/quizzes/:id', quizzes.show);
app.put('/quizzes', quizzes.update);
app.delete('/quizzes', quizzes.delete);
app.get('/answers', answers.index);



