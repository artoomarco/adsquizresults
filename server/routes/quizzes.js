var Quiz = require('../models/quiz').Quiz; 

/*
 * Workouts Routes
 */
exports.index = function(req, res) {
  Quiz.find({}, function(err, docs) {
    if(!err) {
      res.json(200, { quizzes: docs });  
    } else {
      res.json(500, { message: err });
    }
  });
}

exports.show = function(req, res) {
  
  var id = req.params.id; // The id of the workout the user wants to look up. 
  console.log(req.params);
  Quiz.findById(id, function(err, doc) {
    if(!err && doc) {
      res.json(200, doc);
    } else if(err) {
      res.json(500, { message: "Error loading workout." + err});
    } else {
      res.json(404, { message: "Workout not found."});
    }
  });
}

exports.create = function(req, res) {

  var quiz = new Quiz();
  //console.log("req.body.first_name = " + req.body);
  quiz.first_name = req.body.first_name;
  quiz.last_name = req.body.last_name;
  quiz.answer = req.body.answer || [];
    //TODO: get from request
  quiz.total = 100;
  quiz.score = 50;
  quiz.save(function(err){

    if(!err) {
      res.status(201).json({message: "Quiz created: " + quiz });    
    } else {
      res.status(500).json({message: "Could not create quiz. Error: " + err});
    }
  });
}

exports.update = function(req, res) {
  
  var id = req.body.id; 
  quiz.first_name = req.body.first_name;
  quiz.last_name = req.body.last_name;
  quiz.answer = req.body.answer;
  //TODO: get from request
  quiz.total = 100;
  quiz.score = 50;
  Quiz.findById(id, function(err, doc) {
      if(!err && doc) {
        doc.first_name = first_name; 
        doc.last_name = last_name; 
        doc.save(function(err) {
          if(!err) {
            res.json(200, {message: "Quiz updated: " + quiz});    
          } else {
            res.json(500, {message: "Could not update quiz. " + err});
          }  
        });
      } else if(!err) {
        res.json(404, { message: "Could not find quiz."});
      } else {
        res.json(500, { message: "Could not update quiz." + err});
      }
    }); 
}

exports.delete = function(req, res) {

  var id = req.body.id; 
  Quiz.findById(id, function(err, doc) {
    if(!err && doc) {
      doc.remove();
      res.json(200, { message: "Quiz removed."});
    } else if(!err) {
      res.json(404, { message: "Could not find quiz."});
    } else {
      res.json(403, {message: "Could not delete quiz. " + err });
    }
  });
}

exports.last = function(req,res){

  var query = Quiz.find({ }).sort({'date' : -1}).limit(20);
  query.exec(function(err, docs) {
    if(!err) {
      res.json(200, { quizzes: docs });  
    } else {
      res.json(500, { message: err });
    }
  });
}







